from config.path import Path
from modules.reddit import startRedditVideoMaker
import toml
import asyncio
from concurrent.futures import ThreadPoolExecutor
from utils import settings
from fastapi import FastAPI, Depends, HTTPException
from starlette.concurrency import run_in_threadpool
from flask import Flask 
from flask import request
from globals import set_discord_user , set_guildid, set_webhook_url, get_webhook_url, get_discord_user, get_guildid
app = Flask(__name__)

@app.route("/reddit/<subReddit>", methods=["POST"])
def read_item(subReddit):
    data = request.get_json()
    set_discord_user(data.get('userId'))
    set_guildid(data.get('guildId'))
    set_webhook_url(data.get('webhook_url'))
    # print('Guild id => ', get_guildid())
    # print('User id => ', get_discord_user())
    # print('Webhook URL => ', get_webhook_url())
    # return
    config = settings.check_toml(
        f"{Path().main_path}/utils/.config.template.toml", f"{Path.main_path}/config.toml"
    )
    config['reddit']['thread']['subreddit'] = subReddit
    with open(f"{Path().main_path}/config.toml", "w") as f:
        toml.dump(config, f)
    
    asyncio.run(startRedditVideoMaker())

    return {"subReddit": subReddit}

if __name__ == "__main__":
    app.run(debug=True, threaded=True)
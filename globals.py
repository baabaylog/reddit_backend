discord_user = None
guildId = None
webhook_url = None

def set_discord_user(value):
    global discord_user
    discord_user = value

def get_discord_user():
    return discord_user


def set_guildid(value):
    global guildId
    guildId = value

def get_guildid():
    return guildId

def set_webhook_url(value):
    global webhook_url
    webhook_url = value

def get_webhook_url():
    return webhook_url
import os

class Path:
    main_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    utils_path = os.path.join(main_path, 'utils')
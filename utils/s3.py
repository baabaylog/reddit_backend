import boto3
from botocore.exceptions import NoCredentialsError
from tqdm import tqdm
import os
from utils.functions.alerts import send_message
from utils.functions.db_functions import send_discord_data

def upload_to_s3(local_file_path, bucket_name, s3_file_name,user = 'not available', webhook_url = ''):
    s3 = boto3.client('s3')

    file_size = os.path.getsize(local_file_path)
    
    try:
        with tqdm(total=file_size, unit='B', unit_scale=True, desc='Uploading', position=0, leave=True) as pbar:
            s3.upload_file(local_file_path, bucket_name, s3_file_name, Callback=lambda bytes_uploaded: pbar.update(bytes_uploaded))
        print("Upload Successful")
        
        send_message(f'<@{user}> your video is ready to download please get link by running /video_link', webhook_url)
        link = generate_shareable_link(bucket_name, s3_file_name)

        print('File link generated => ',link)
        return send_discord_data(user,link)
    except FileNotFoundError:
        print("The file was not found")
        return None
    except NoCredentialsError:
        print("Credentials not available")
        return None

def generate_shareable_link(bucket_name, s3_file_name):
    s3 = boto3.client('s3')
    
    try:
        response = s3.generate_presigned_url('get_object',
                                            Params={'Bucket': bucket_name, 'Key': s3_file_name},
                                            ExpiresIn=3600)  # URL valid for 1 hour
        return response
    except Exception as e:
        print(f"Error generating presigned URL: {e}")
        return None



import requests
import json

def send_discord_data(discord_id, video_link):


    url = "http://localhost:3000/save-video-link"

    payload = json.dumps({"discord_id": discord_id,"video_link": video_link})
    headers = {'Content-Type': 'application/json'}



    try:
        response = requests.request("POST", url, headers=headers, data=payload)

        print(response.text)

        print("POST request successful!")
        print("Response:", response.text)
    except requests.exceptions.HTTPError as errh:
        print ("HTTP Error:", errh)
    except requests.exceptions.ConnectionError as errc:
        print ("Error Connecting:", errc)
    except requests.exceptions.Timeout as errt:
        print ("Timeout Error:", errt)
    except requests.exceptions.RequestException as err:
        print ("Something went wrong:", err)
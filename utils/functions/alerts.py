import requests

default_webhook_url = 'https://discord.com/api/webhooks/1184759221267480638/SEebo_W_HR-PzXBcGn3k9-btznSPXxHECvpfZ-C4muk9fEpT73M7x9bIKG6m2fymL-6s'


def send_message(content, webhook_url ):
    try:
        
        print(webhook_url)
        payload = {'content': content}
        response = requests.post(webhook_url, json=payload)
        
        if response.status_code == 204:
            print('Message sent successfully!')
        else:
            print(f'Error sending message. Status code: {response.status_code}')
            print(response.text)
    except Exception as e:
        print(f'Error: {str(e)}')


